﻿// Standard
using System.Threading.Tasks;

// Internal
using Eventflow.Contracts;

namespace WebhookGateway.Interfaces
{
    public interface IEventLogHub
    {
        Task PushEventLog(EventFlowLog log);
    }

}
