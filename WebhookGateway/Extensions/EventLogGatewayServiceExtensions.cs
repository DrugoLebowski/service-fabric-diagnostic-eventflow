﻿// Standard
using System;

// Vendor
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

// Internal
using WebhookGateway.Interfaces;
using WebhookGateway.Services;
using WebhookGateway.Services.Options;

namespace WebhookGateway.Extensions
{
    public static class EventLogGatewayServiceExtensions
    {
        public static IServiceCollection AddEventLogGatewayService(this IServiceCollection collection, Action<IOptions<EventLogGatewayServiceOptions>> setupAction)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (setupAction == null) throw new ArgumentNullException(nameof(setupAction));

            collection.Configure(setupAction);

            return collection.AddTransient<IEventLogGatewayService, EventLogGatewayService>();
        }
    }
}
