﻿// Standard
using System.Threading.Tasks;

// Vendor
using Microsoft.AspNetCore.SignalR;

// Internal
using WebhookGateway.Interfaces;

namespace WebhookGateway.Hubs
{
    public class EventLogHub : Hub<IEventLogHub>
    {
        public async Task SubscribeToGroup(string group)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, group);
        }

        public async Task UnsubscribeToGroup(string group)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, group);
        }
    }
}
