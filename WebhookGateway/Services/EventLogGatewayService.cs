﻿// Standard
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// Vendor
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

// Internal
using Eventflow.Contracts;
using WebhookGateway.Hubs;
using WebhookGateway.Interfaces;

namespace WebhookGateway.Services
{
    public class EventLogGatewayService : IEventLogGatewayService
    {
        private ILogger<EventLogGatewayService> Logger { get; }
        private IHubContext<EventLogHub, IEventLogHub> EventLogHub { get; }

        public EventLogGatewayService(ILogger<EventLogGatewayService> logger, IHubContext<EventLogHub, IEventLogHub> eventLogHub)
        {
            Logger = logger;
            EventLogHub = eventLogHub;
        }

        public async Task PushEventLogsToClients(IList<EventFlowLog> logs)
        {
            using (Logger.BeginScope($"PushEventLogsToClient(...)"))
            {
                await PushEventLogsByLogLevel(logs);
                await PushEventLogsByProvider(logs);
            }
        }

        private async Task PushEventLogsByLogLevel(IEnumerable<EventFlowLog> logs)
        {
            Logger.LogInformation($"PushEventLogsByLogLevel(logs={logs}) called.");

            foreach (var log in logs)
            {
                if (log.Level.HasValue)
                    await EventLogHub.Clients.Group($"{log.Level.Value}").PushEventLog(log);

                await EventLogHub.Clients.All.PushEventLog(log);
            }
        }

        private async Task PushEventLogsByProvider(IEnumerable<EventFlowLog> logs)
        {
            Logger.LogInformation($"PushEventLogsByProvider(logs={logs}) called.");

            foreach (var log in logs)
            {
                if (log.Level.HasValue)
                    await EventLogHub.Clients.Group($"{log.ProviderName}").PushEventLog(log);

                await EventLogHub.Clients.All.PushEventLog(log);
            }
        }
    }
}
