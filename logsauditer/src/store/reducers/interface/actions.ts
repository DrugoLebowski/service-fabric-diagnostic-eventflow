// Internal
import { Actions, UpdateToggleMenu } from "./types";

/**
 * Used to toggle the lateral menu in one of its two states (opened/closed).
 * 
 * @param statoMenu The state to turn the menu
 */
export function toggleMenu(statoMenu: boolean) {
  return {
    type: Actions.TOGGLE_MENU,
    payload: statoMenu,
  } as UpdateToggleMenu;
}