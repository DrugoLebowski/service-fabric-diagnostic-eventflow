// Internal
import { EventFlowLog } from "@models/event-flow-log";

export interface LogsState {
  logs: Array<EventFlowLog>;
}

export const Actions = {
  AddLogs: "@ADD_LOGS",
}

export interface AddLogsAction { 
  type: typeof Actions.AddLogs;
  payload: Array<EventFlowLog>;
}

export type ActionTypes = AddLogsAction;