// Internal
import { EventFlowLog } from "@models/event-flow-log";
import { AddLogsAction, Actions } from "./types";

export const addLogs = (logs: Array<EventFlowLog>): AddLogsAction => ({
  type: Actions.AddLogs,
  payload: logs
});
