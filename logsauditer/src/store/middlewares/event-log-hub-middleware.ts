// Vendor
import * as SignalR from "@aspnet/signalr";

// Internal
import Config from "config";
import { EventFlowLog } from "@models/event-flow-log";
import { addLogs } from "@store/reducers/logs/actions";

export const Actions = {
  SubscribeToGroup: "@SUBSCRIBE_TO_GROUP",
};

export interface Subscribe {
  type: typeof Actions.SubscribeToGroup;
  payload: Array<string>;
}

export type ActionTypes = Subscribe;

const connection = new SignalR.HubConnectionBuilder()
  .withUrl(`${Config.host}/audit`)
  .build();

export const eventLogHubInvokeMiddleware = (store: any) => (next: Function) => async (action: ActionTypes) => {
  switch (action.type) {
    case Actions.SubscribeToGroup:
      action.payload
        .forEach(group => connection.invoke("SubscribeToGroup", group));
      break;
  }

  return next(action);
}

export async function eventLogHubCommandsMiddleware(store: any) {
  connection.on("PushEventLog", (log: EventFlowLog) => {

    addLogs([ log ]);

    console.info(`PushEventLog(logs=${JSON.stringify(log)}) called.`)
  });

  connection
    .start()
    .then(() => {
      console.info(`HubEventLog connected.`);
    });
}


