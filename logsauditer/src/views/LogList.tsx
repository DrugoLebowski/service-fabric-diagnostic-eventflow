// Vendor
import React from "react";
import { connect } from "react-redux";
import { makeStyles, createStyles, Theme, Typography } from "@material-ui/core";

// Internal
import { AppState } from "@store/index";
import clsx from "clsx";

interface Props {
  statoMenu: boolean;
}

const drawerWidth = 300;

const useStyles = makeStyles((theme: Theme) => createStyles({
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
}));

function LogList(props: Props) {

  const classes = useStyles();

  return (
    <main className={clsx(classes.content, {
      [classes.contentShift]: props.statoMenu,
    })}>
      <div className={classes.drawerHeader} />
      <Typography paragraph>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor rerum provident doloribus repellendus ullam culpa dicta corporis optio molestias voluptatibus quos, hic fugit, aliquid porro maiores iure? Recusandae, voluptatibus sequi?
      </Typography>
    </main>
  );
}

export default connect((state: AppState) => ({
  statoMenu: state.interfaceReducer.statoMenu,
}), {})(LogList);