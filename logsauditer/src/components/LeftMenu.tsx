// Vendor
import React from "react";
import { connect } from "react-redux";
import {
  makeStyles,
  createStyles,
  useTheme,
  Theme,
  Divider,
} from "@material-ui/core";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

// Internal
import { AppState } from "@store/index";
import { toggleMenu } from "@store/reducers/interface/actions";

interface Props {
  statoMenu: boolean;
  toggleMenu: typeof toggleMenu;
}

const drawerWidth = 300;

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerHeader: {
      display: "flex",
      alignItems: "center",
      padding: "0 8px",
      ...theme.mixins.toolbar,
      justifyContent: "flex-end",
    },
  }),
);

function LeftMenu(props: Props) {
  const classes = useStyle();
  const theme = useTheme();

  const toggleMenu = () => props.toggleMenu(!props.statoMenu);

  return (
    <Drawer
      anchor="left"
      classes={{
        paper: classes.drawerPaper,
      }}
      className={classes.drawer}
      open={props.statoMenu}
      variant="persistent"
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={toggleMenu}>
          {theme.direction === "ltr" ? (
            <ChevronLeftIcon />
          ) : (
            <ChevronRightIcon />
          )}
        </IconButton>
      </div>
      <Divider />
    </Drawer>
  );
}

export default connect(
  (state: AppState) => ({
    statoMenu: state.interfaceReducer.statoMenu,
  }),
  {
    toggleMenu,
  },
)(LeftMenu);
