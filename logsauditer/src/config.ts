interface ConfigEnv {
  host: string;
}

interface Config {
  [index: string]: ConfigEnv;
}

const config: Config = {
  "development": {
    host: "http://localhost:8512",
  },
  "staging": {
    host: "http://localhost:8512",
  },
  "production": {
    host: "http://localhost:8512",
  },
}

export default config[process.env.NODE_ENV] as ConfigEnv;
