﻿// Internal
using System;

// Vendor
using Newtonsoft.Json;

namespace Eventflow.Contracts
{
    public partial class EventFlowLog
    {
        [JsonProperty("Timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Timestamp { get; set; }

        [JsonProperty("ProviderName", NullValueHandling = NullValueHandling.Ignore)]
        public string ProviderName { get; set; }

        [JsonProperty("Level", NullValueHandling = NullValueHandling.Ignore)]
        public long? Level { get; set; }

        [JsonProperty("Keywords", NullValueHandling = NullValueHandling.Ignore)]
        public long? Keywords { get; set; }

        [JsonProperty("Payload", NullValueHandling = NullValueHandling.Ignore)]
        public Payload Payload { get; set; }
    }
}