﻿// Vendor
using Newtonsoft.Json;

namespace Eventflow.Contracts
{
    public partial class Payload
    {
        [JsonProperty("Message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        [JsonProperty("EventId", NullValueHandling = NullValueHandling.Ignore)]
        public long? EventId { get; set; }

        [JsonProperty("Scope", NullValueHandling = NullValueHandling.Ignore)]
        public string Scope { get; set; }
    }
}
