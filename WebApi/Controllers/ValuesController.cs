﻿// Standard
using System.Threading.Tasks;

// Vendor
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ValuesController : ControllerBase
    {
        private ILogger<ValuesController> Logger { get; }

        public ValuesController(ILogger<ValuesController> logger)
        {
            Logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] string value)
        {
            using (var logContext = Logger.BeginScope("Test log con Eventflow"))
            {
                Logger.LogDebug($"Chiamata ValuesController.Post(value={value})");
                Logger.LogInformation($"Chiamata ValuesController.Post(value={value})");
                Logger.LogWarning($"Chiamata ValuesController.Post(value={value})");
                Logger.LogError($"Chiamata ValuesController.Post(value={value})");
            }
            
            return await Task.FromResult(Ok());
        }
    }
}
